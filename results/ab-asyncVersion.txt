Benchmarking localhost (be patient)
Completed 100 requests
Completed 200 requests
Finished 250 requests


Server Software:        
Server Hostname:        localhost
Server Port:            8082

Document Path:          /auth?username=matt&password=password
Document Length:        2 bytes

Concurrency Level:      20
Time taken for tests:   3.738 seconds
Complete requests:      250
Failed requests:        0
Keep-Alive requests:    250
Total transferred:      57250 bytes
HTML transferred:       500 bytes
Requests per second:    66.89 [#/sec] (mean)
Time per request:       299.005 [ms] (mean)
Time per request:       14.950 [ms] (mean, across all concurrent requests)
Transfer rate:          14.96 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       1
Processing:    61  283  41.8    292     310
Waiting:       60  283  41.9    292     310
Total:         61  283  41.7    292     310

Percentage of the requests served within a certain time (ms)
  50%    292
  66%    295
  75%    297
  80%    299
  90%    302
  95%    304
  98%    308
  99%    310
 100%    310 (longest request)