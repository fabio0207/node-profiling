# Teste

## Teste sincrono:

Subir a aplicação em modo profiling:
```
NODE_ENV=production node --prof src/syncVersion.js
```

Chamar a rota de criação de usuário e teste de carga:
```
curl -X GET "http://localhost:8082/newUser?username=matt&password=password"
ab -k -c 20 -n 250 "http://localhost:8082/auth?username=matt&password=password"
```

> ab => ApacheBench

Para gerar o arquivo legível a humanos execute:
```
node --prof-process isolate-0xnnnnnnnnnnnn-v8.log > processed-syncVersion.txt
```

## Teste assíncrono

Subir a aplicação em modo profiling:
```
NODE_ENV=production node --prof src/asyncVersion.js
```

Chamar a rota de criação de usuário e teste de carga:
```
curl -X GET "http://localhost:8082/newUser?username=matt&password=password"
ab -k -c 20 -n 250 "http://localhost:8082/auth?username=matt&password=password"
```

Para gerar o arquivo legível a humanos execute:
```
node --prof-process isolate-0xnnnnnnnnnnnn-v8.log > processed-asyncVersion.txt
```

> Documentação de referência: https://nodejs.org/en/docs/guides/simple-profiling/